//
//  KeyCenter.swift
//  MarvelApp
//
//  Created by mohamed on 17/01/2021.
//

import Foundation
class KeyCenter {
    let privateKey: String
    let apiKey: String
    
    static let instance = KeyCenter(
        privateKey: "76cb8d0bd88b49abe3e4d049e6064518062f998c",
        apiKey: "1951bc8fc24c16592930f688c6df1581"
    )
    
    init(privateKey: String, apiKey: String) {
        self.privateKey = privateKey
        self.apiKey = apiKey
    }
    
}
