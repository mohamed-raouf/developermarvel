//
//  CacheObject.swift
//  MarvelApp
//
//  Created by mohamed on 19/01/2021.
//

import UIKit
class CacheObject {  //NSObject,NSCoding
    
//    func encode(with coder: NSCoder) {
//        coder.encode(value, forKey: "value")
////        coder.encode(expiryDate, forKey: "expiryDate")
//    }
//
    
    
    let value: [Character]
//    let expiryDate: NSDate
    
    /**
     *  Designated initializer.
     *
     *  @param value            An object that should be cached
     *  @param expiryDate    The expiry date of the given value
     */
    init(value: [Character]) { //, expiryDate: NSDate
        self.value = value
//        self.expiryDate = expiryDate
    }
//    func encode(with coder: NSCoder) {
//        coder.encode(value, forKey: "value")
//    }
//    
//    required init?(coder: NSCoder) {
//        value = coder.decodeObject(forKey: "value") as! [Character]
//
//    }
    
    /**
     *  Returns true if this object is expired.
     *  Expiry of the object is determined by its expiryDate.
     */
//    func isExpired() -> Bool {
////        let expires = expiryDate.timeIntervalSinceNow
//        let now = NSDate().timeIntervalSinceNow
//
//        return now > expires
//    }
    
    
    /// NSCoding
//    required init(coder aDecoder: NSCoder) {
//        value = aDecoder.decodeObject(forKey: "value") as! [Character]
////        expiryDate = aDecoder.decodeObject(forKey: "expiryDate") as! NSDate
//    }
    
//    func encodeWithCoder(aCoder: NSCoder!) {
//        aCoder.encode(value, forKey: "value")
////        aCoder.encode(expiryDate, forKey: "expiryDate")
//    }
}

//class CacheObject : NSObject, NSCoding {
//
//    func encode(with coder: NSCoder) {
//        coder.encode(value, forKey: "value")
////        coder.encode(expiryDate, forKey: "expiryDate")
//    }
//
//
//
//    let value: [Character]
////    let expiryDate: NSDate
//
//    /**
//     *  Designated initializer.
//     *
//     *  @param value            An object that should be cached
//     *  @param expiryDate    The expiry date of the given value
//     */
//    init(value: [Character]) { //, expiryDate: NSDate
//        self.value = value
////        self.expiryDate = expiryDate
//    }
//
//    /**
//     *  Returns true if this object is expired.
//     *  Expiry of the object is determined by its expiryDate.
//     */
////    func isExpired() -> Bool {
//////        let expires = expiryDate.timeIntervalSinceNow
////        let now = NSDate().timeIntervalSinceNow
////
////        return now > expires
////    }
//
//
//    /// NSCoding
//    required init(coder aDecoder: NSCoder) {
//        value = aDecoder.decodeObject(forKey: "value") as! [Character]
////        expiryDate = aDecoder.decodeObject(forKey: "expiryDate") as! NSDate
//    }
//
//    func encodeWithCoder(aCoder: NSCoder!) {
//        aCoder.encode(value, forKey: "value")
////        aCoder.encode(expiryDate, forKey: "expiryDate")
//    }
//}
