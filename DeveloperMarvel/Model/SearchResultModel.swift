//
//  SearchResultModel.swift
//  DeveloperMarvel
//
//  Created by mohamed on 21/01/2021.
//

import Foundation

struct SearchResultModel {
    let name : String?
    let imageUrl:URL?
    
    init(name : String , imageUrl:URL) {
        self.name = name
        self.imageUrl = imageUrl
    }
}
