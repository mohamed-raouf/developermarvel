//
//  UrlsList.swift
//  DeveloperMarvel
//
//  Created by mohamed on 21/01/2021.
//

import Foundation

struct UrlsList {
    let type : String?
    let url : String?
    
    enum CodingKeys: String, CodingKey {
        case type
        case url
        
    }
    
}


extension UrlsList: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
    }
}

extension UrlsList: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(url, forKey: .url)
        try container.encodeIfPresent(url, forKey: .url)
    }
}
