//
//  Action.swift
//  MarvelApp
//
//  Created by mohamed on 17/01/2021.
//

import Foundation
// MARK: - Action
struct Action: Codable {
    let id, bgColor: String?
    let endpointURL: String?
    let key, label, method, type: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case bgColor = "bg_color"
        case endpointURL = "endpoint_url"
        case key, label, method, type
    }
}
