//
//  CharacterCatalogVC.swift
//  DeveloperMarvel
//
//  Created by mohamed on 20/01/2021.
//

import UIKit

class CharacterCatalogVC: UIViewController {
    
    @IBOutlet weak var characterCatalogTableView: UITableView!{
        didSet{
            self.characterCatalogTableView.delegate = self
            self.characterCatalogTableView.dataSource = self
            characterCatalogTableView.register(HeaderTableViewCell.nib, forCellReuseIdentifier: HeaderTableViewCell.identifier)
            
            characterCatalogTableView.register(MiddleTableViewCell.nib, forCellReuseIdentifier: MiddleTableViewCell.identifier)
            characterCatalogTableView.register(RelatedLinksTVC.nib, forCellReuseIdentifier: RelatedLinksTVC.identifier)
        }
    }
    var character : Character!
    var urlIndex = 0
    var isScroll = false
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //        self.navigationController?.navigationBar.addSubview(visualEffectView)
        self.navigationItem.title = "Whatever..."
        self.navigationController?.navigationBar.tintColor = UIColor.white
        //
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        
        
        let searchIcon =  #imageLiteral(resourceName: "icn-nav-search")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: searchIcon, landscapeImagePhone: searchIcon, style: .plain, target: self, action: #selector(goToSearch))
        
        
        
        self.characterCatalogTableView.reloadData()
    }
    
    @objc func goToSearch(){
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Restore the navigation bar to default
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
    }
    
}



extension CharacterCatalogVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4 + (self.character?.urls!.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell:HeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.identifier) as! HeaderTableViewCell
            //            let url = URL(string:"http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")!
            
            cell.setupCell(characterImageUrl: (self.character.thumbnail?.url!)!, name: self.character.name!, description: self.character.description ?? "")
            cell.delegate = self
            return cell
        }else if indexPath.row == 1{
            let cell:MiddleTableViewCell = tableView.dequeueReusableCell(withIdentifier: MiddleTableViewCell.identifier) as! MiddleTableViewCell
            
            cell.setupCell(title : "Comics", catalog: (self.character.comics?.items)!, catalogType: .comics)
            return cell
            
        }else if indexPath.row == 2{
            let cell:MiddleTableViewCell = tableView.dequeueReusableCell(withIdentifier: MiddleTableViewCell.identifier) as! MiddleTableViewCell
            
            cell.setupCell(title : "series", catalog: (self.character.series?.items)!, catalogType: .series)
            return cell
        }else if indexPath.row == 3{
            let cell:MiddleTableViewCell = tableView.dequeueReusableCell(withIdentifier: MiddleTableViewCell.identifier) as! MiddleTableViewCell
            
            cell.setupCell(title : "events", catalog: (self.character.events?.items)!, catalogType: .events)
            return cell
        }else if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7{
            let cell:RelatedLinksTVC = tableView.dequeueReusableCell(withIdentifier: RelatedLinksTVC.identifier) as! RelatedLinksTVC
            
            cell.setupCell(urlTitle: (self.character?.urls![urlIndex].type)!)
            self.urlIndex += 1
            return cell
        }else{
            self.urlIndex -= 1
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3{
            return 200
        }else if indexPath.row == 0{
            return 300
        }else{
            return 60
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
            navigationController?.setNavigationBarHidden(false, animated: true)
            self.isScroll = true
            self.urlIndex = 0
        } else {
            navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 0
    //    }
    //    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //        return 0
    //    }
}

extension CharacterCatalogVC : HeaderTableViewCellBackDelegate{
    func onBackButton() {
        //        dismiss(animated: true, completion: nil)
        self.dismissDetail()
    }
    
    
}


// MARK: - Create
extension CharacterCatalogVC{
    class func create() -> CharacterCatalogVC {
        let view = CharacterCatalogVC()
        return view
    }
}
