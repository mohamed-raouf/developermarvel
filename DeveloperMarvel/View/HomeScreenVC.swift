//
//  HomeScreenVC.swift
//  DeveloperMarvel
//
//  Created by mohamed on 19/01/2021.
//

import UIKit
import PureLayout
import CCBottomRefreshControl
import PINCache

class HomeScreenVC: UIViewController , HomeScreenPresenterDelegate{
    
    //    func search(name: String?, page: Int?) {
    //        presenter.search(name: name, page: page)
    //    }
    
    
    // MARK: - Variables
    private var characters = [Character]()
    private var dataList = [SearchResultModel]()
    //    let repository : CharactersRepository =  CharactersRepository.init(network: Network())
    private var characterData: CharacterDataWrapper? {
        didSet { self.didUpdateCharacterData() }
    }
    var refreshControl = UIRefreshControl()
    private var presenter: HomeScreenPresenter = HomeScreenPresenter(repository : CharactersRepository.init(network: Network()))
    
    private lazy var bottomRefreshControl: UIRefreshControl = {
        let control = UIRefreshControl.newAutoLayout()
        control.accessibilityIdentifier = "catalog_refresh_control_bottom"
        control.triggerVerticalOffset = 100
        control.addTarget(self, action: #selector(loadNextPage), for: .valueChanged)
        return control
    }()
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.register(SearchResultTVC.nib, forCellReuseIdentifier: SearchResultTVC.identifier)
        }
    }
    //    var tableView : UITableView! {
    //        didSet{
    //            self.tableView = UITableView(frame: CGRect.zero)
    //            self.tableView.delegate = self
    //            self.tableView.dataSource = self
    //            self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    //
    //        }
    //    }
    // Search controller
    var searchController : UISearchController = ({
        let controller = UISearchController(searchResultsController: nil)
        
        //        controller.delegate = self
        //        controller.searchBar.delegate = self
        //        controller.searchResultsUpdater = self
        controller.dimsBackgroundDuringPresentation = false
        controller.hidesNavigationBarDuringPresentation = true
        controller.searchBar.sizeToFit()
        //        controller.searchBar = SearchBarTableList
        
        return controller
    })()
    //    var searchBar = SearchBarTableList()
    private let itemsPerPage: Int = 20
    private var currentPage: Int = 0
    private var currentName: String?
    var isFirstLoad: Bool {
        return currentPage == 0
    }
    
    var isSearching: Bool = false
    let tableViewCellIdentifier = "cellID"
    private lazy var loadingViewController: LoadingViewController = {
        return LoadingViewController.loadXib(from: nil)
    }()
    // MARK: - Outlets
    @IBOutlet weak var CharactersCollectionView: UICollectionView! {
        didSet{
            let layout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: 400.0, height: 200.0)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.sectionInset = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 5.0, right: 5.0)
            
            self.CharactersCollectionView.delegate = self
            self.CharactersCollectionView.dataSource = self
            self.CharactersCollectionView.registerCell(of: CatalogItemCollectionViewCell.self)
            self.CharactersCollectionView.collectionViewLayout = layout
        }
    }
    
    // MARK: - Funtions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavbar()
        self.view.backgroundColor = .black
        presenter.setViewDelegate(self)
        loadCharacters(name: "", at: 0)
        //        tableView = UITableView()
        //        self.tableView = UITableView(frame: CGRect.zero)
        //        self.tableView.delegate = self
        //        self.tableView.dataSource = self
        //        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //        self.navigationItem.searchController?.searchBar = searchBar
        //        tableView?.delegate = self
        //        tableView?.dataSource = self
        configureViews()
    }
    func configureViews() {
        CharactersCollectionView.bottomRefreshControl = bottomRefreshControl
    }
    fileprivate func setupNavbar() {
        self.navigationController?.navigationBar.barTintColor = .black
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = #imageLiteral(resourceName: "icn-nav-marvel")
        imageView.image = image
        navigationItem.titleView = imageView
        //        searchBar.placeholder = "Your placeholder"
        let searchIcon =  #imageLiteral(resourceName: "icn-nav-search")
        //        let rightNavBarButton = UIBarButtonItem(customView:searchBar)
        //        rightNavBarButton.image = searchIcon
        //        self.navigationItem.leftBarButtonItem = rightNavBarButton
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: searchIcon, landscapeImagePhone: searchIcon, style: .plain, target: self, action: #selector(goToSearch))
    }
    private func loadDataFromCache(name:String? = nil){
        let decoded  = UserDefaults.standard.data(forKey: "characters_save")
        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! CacheObject
        
        
        PINCache.shared.object(forKey: "characters_cache") { (cache, key, object) in
            if let character = object as? [Character] {
                if name != "" && name != nil{
                    self.characters = character.filter({ $0.name == name })
                }else{
                    self.characters = character
                }
                
            }
        }
    }
    func loadCharacters(name: String?, at page: Int) {
        if !ReachabilityConnect.isConnectedToNetwork() {
            loadDataFromCache(name: name)
            return
        }
        
        //        guard self.currentPage != page else {
        //            return
        //        }
        var pagging = page
        if page > 0 {
            pagging = pagging * 20
        }
        
        presenter.loadCharacters(name: name, at: pagging)
    }
    
    func getData(characterData: (CharacterDataWrapper)) {
        stopLoading()
        //        self.currentPage = currentPage
        //        self.currentName = currentName
        if isSearching {
            guard let data = characterData.data?.results else {return}
            for result in data {
                self.dataList.append(SearchResultModel(name: result.name!, imageUrl: (result.thumbnail?.url!)!))
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }else{
            self.characterData = characterData
        }
        print(characterData.data?.count)
    }
    
    @objc private func goToSearch(){
        //        navigationItem.titleView = searchBar;
        
        // Create the search controller and specify that it should present its results in this same view
        searchController = UISearchController(searchResultsController: nil)
        // Set any properties (in this case, don't hide the nav bar and don't show the emoji keyboard option)
        searchController.hidesNavigationBarDuringPresentation = false
        //            searchController.searchBar.keyboardType = UIKeyboardType.ASCIICapable
        
        // Make this class the delegate and present the search
        self.searchController.searchBar.delegate = self
        present(searchController, animated: true, completion: nil)
        buildSearchTableView()
    }
    
    @objc func loadNextPage() {
        //        viewModel.loadNextPage()
        self.currentPage += 1
        loadCharacters(name: currentName, at: (currentPage))
        
    }
    func showLoading() {
        add(loadingViewController)
        loadingViewController.view.autoPinEdgesToSuperviewEdges()
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            self.loadingViewController.remove()
            self.CharactersCollectionView.bottomRefreshControl?.endRefreshing()
        }
        
    }
    
    private func didUpdateCharacterData() {
        
        if currentPage >= 1 {
            characters.append(contentsOf: characterData?.data?.results ?? [])
        } else {
            characters = characterData?.data?.results ?? []
        }
        PINCache.shared.setObject(characters, forKey: "characters_cache")
        DispatchQueue.main.async {
            
            self.CharactersCollectionView.reloadData()
        }
        
    }
    private func  moveToCatalog(character : Character){
        let characterCatalogVC = CharacterCatalogVC()
        characterCatalogVC.character = character
        self.presentDetail(characterCatalogVC)
        
    }
    
}
// MARK: - Extension

// MARK: - UICollectionView
extension HomeScreenVC : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(CatalogItemCollectionViewCell.self, for: indexPath)
        cell.indexPath = indexPath
        //        let viewModel =  CatalogItemCollectionViewCellDTO(title: self.characters[indexPath.row].name, imageURL:URL(string: "\(self.characters[indexPath.row].thumbnail?.path).\( self.characters[indexPath.row].thumbnail?.ext)") , favorited: false)
        //        cell.delegate = self
        let viewModel =  CatalogItemCollectionViewCellDTO(character: self.characters[indexPath.row], favorited: false)
        cell.configure(with: viewModel)
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        moveToCatalog(character: self.characters[indexPath.row])
    }
    
    
}

// MARK: - SearchTableView
extension HomeScreenVC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: TableView creation and updating
    
    // Create SearchTableview
    func buildSearchTableView() {
        self.tableView.isHidden = false
        self.view.bringSubviewToFront(tableView)
        //        tableView = UITableView(frame: CGRect.zero)
        //        tableView?.delegate = self
        //        tableView?.dataSource = self
        //        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: "CustomSearchTextFieldCell")
        
        //        if let tableView = tableView {
        //            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CustomSearchTextFieldCell")
        //            tableView.delegate = self
        //            tableView.dataSource = self
        //            //self.window?.addSubview(tableView)
        //
        //        } else {
        //            addData()
        //            print("tableView created")
        //            tableView = UITableView(frame: CGRect.zero)
        //        }
        
        updateSearchTableView()
    }
    
    // Updating SearchtableView
    func updateSearchTableView() {
        
        //        if let tableView = tableView {
        //            self.view.bringSubviewToFront(tableView)
        //view.bringSubviewToFront(tableView)
        //            var tableHeight: CGFloat = CGFloat(0) //0
        //            if dataList.count < 5 {
        //                tableHeight = tableView.contentSize.height
        //            }else {
        //                tableHeight = CGFloat(250)
        //            }
        //
        //            //tableHeight = CGFloat(250) //tableView.contentSize.height
        //
        //            // Set a bottom margin of 10p
        //            if tableHeight < tableView.contentSize.height {
        //                tableHeight -= 10
        //            }
        //
        //            // Set tableView frame
        //            var frame  = self.view.frame.origin //searchBar.frame.origin
        //            var tableViewFrame = CGRect(x: 0, y:frame.y  , width: self.view.frame.width - 4, height: tableHeight)
        //            //tableViewFrame.origin = searchBar.convert(tableViewFrame.origin, to: nil)
        //            tableViewFrame.origin.x += 2
        //            tableViewFrame.origin.y += frame.x + self.view.frame.height
        //            UIView.animate(withDuration: 0.2, animations: { [weak self] in
        //                self?.tableView?.frame = tableViewFrame
        //            })
        
        //Setting tableView style
        tableView.layer.masksToBounds = true
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.layer.cornerRadius = 5.0
        tableView.separatorColor = UIColor.lightGray
        tableView.backgroundColor = UIColor.white.withAlphaComponent(1) // 0.4
        tableView.separatorStyle = .none
        
        if self.isFirstResponder {
            // mohamed
            //                self.view.bringSubviewToFront(self)
            self.view.bringSubviewToFront(tableView)
            //view.bringSubviewToFront(searchBar)
            
        }
        //            tableView.reloadData()
        //        }
    }
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(dataList.count)
        return dataList.count
    }
    
    // MARK: TableViewDelegate methods
    
    //Adding rows in the tableview with the data from dataList
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTVC", for: indexPath) as! SearchResultTVC
        //        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        //        let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        
        //        cell.backgroundColor = UIColor.clear
        print(dataList[indexPath.row])
        //        cell.textLabel?.attributedText = dataList[indexPath.row]
        cell.setupCell(searchResult: dataList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row")
        //        self.text = resultsList[indexPath.row].getStringText()
        //        self.text = self.text
        tableView.isHidden = true
        self.view.endEditing(true)
        self.isSearching = false
    }
    
    
    // MARK: Early testing methods
    func addData(){
        //        let a = SearchData(context: context)
        //        a.search_title = self.text!
        //        dataList.append(a)
        //        saveItems()
    }
    
}


// MARK: - SearchBar

extension HomeScreenVC : UISearchBarDelegate , UISearchResultsUpdating , UISearchControllerDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        print("search update")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text == "" || searchBar.text == " " {
            return
        }
        self.isSearching = true
        loadCharacters(name: searchBar.text, at: 0)
        self.tableView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.tableView.isHidden = true
        self.isSearching = false
    }
}

// MARK: - Create
extension HomeScreenVC{
    class func create() -> HomeScreenVC {
        let view = HomeScreenVC()
        return view
    }
}


