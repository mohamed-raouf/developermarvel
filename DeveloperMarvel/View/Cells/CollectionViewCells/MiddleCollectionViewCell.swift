//
//  MiddleCollectionViewCell.swift
//  DeveloperMarvel
//
//  Created by mohamed on 20/01/2021.
//

import UIKit

class MiddleCollectionViewCell: UICollectionViewCell {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView.newAutoLayout()
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        return indicator
    }()
    @IBOutlet weak var catalogImage: UIImageView!
    
    @IBOutlet weak var catalogTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    func setupCell(catalogImage:URL , catalogTitle:String){
        catalogTitleLabel.text = catalogTitle
    }
}


private extension MiddleCollectionViewCell {
    
    private func setupActivityIndicator() {
        
        addSubview(activityIndicator)
        bringSubviewToFront(activityIndicator)
        
        activityIndicator.autoCenterInSuperview()
        activityIndicator.autoSetDimensions(to: CGSize(width: 22.0, height: 22.0))
        activityIndicator.startAnimating()
        
    }
    
    private func removeActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
}
