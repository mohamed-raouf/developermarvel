//
//  RelatedLinksTVC.swift
//  DeveloperMarvel
//
//  Created by mohamed on 20/01/2021.
//

import UIKit

class RelatedLinksTVC: UITableViewCell {

    @IBOutlet weak var urlLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.accessoryType = .disclosureIndicator
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func setupCell(urlTitle:String){
        self.urlLabel.text = urlTitle
    }
    
    
}
