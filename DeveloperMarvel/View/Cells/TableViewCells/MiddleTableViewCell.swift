//
//  MiddleTableViewCell.swift
//  DeveloperMarvel
//
//  Created by mohamed on 20/01/2021.
//

import UIKit
enum CatalogType{
    case comics
    case series
    case stories
    case events
}
class MiddleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var catalogTitleLabel: UILabel!
    @IBOutlet weak var catalogCollectionView: UICollectionView!{
        didSet{
            self.catalogCollectionView.delegate = self
            self.catalogCollectionView.dataSource = self
            self.catalogCollectionView.register(MiddleCollectionViewCell.nib, forCellWithReuseIdentifier: MiddleCollectionViewCell.identifier)
        }
    }
    var catalogType : CatalogType?
    var catalog : [Any]?{
        didSet{
            DispatchQueue.main.async {
                self.catalogCollectionView.reloadData()
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    func setupCell(title :String, catalog : [Any] , catalogType : CatalogType ){
        self.catalogTitleLabel.text = title
        self.catalogType = catalogType
        switch catalogType {
        case .comics:
            self.catalog = catalog as? [ComicSummary]
        case .events:
            self.catalog = catalog as? [EventSummary]
        case .series:
            self.catalog = catalog as? [SeriesSummary]
        case .stories:
            self.catalog = catalog as? [StorySummary]
        default:
            print("dd")
        }
        
    }
}

extension MiddleTableViewCell : UICollectionViewDataSource , UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catalog?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(MiddleCollectionViewCell.self, for: indexPath)
        switch self.catalogType {
        case .comics:
            let newCatalog = catalog as? [ComicSummary]
            //        let cat = catalog as? [ComicSummary]
            cell.setupCell(catalogImage: URL(string:"http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")!, catalogTitle: newCatalog?[indexPath.row].name ?? "")
            return cell
            
        case .events:
            let newCatalog = catalog as? [EventSummary]
            //        let cat = catalog as? [ComicSummary]
            cell.setupCell(catalogImage: URL(string:"http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")!, catalogTitle: newCatalog?[indexPath.row].name ?? "")
            return cell
            
        case .series:
            let newCatalog = catalog as? [SeriesSummary]
            //        let cat = catalog as? [ComicSummary]
            cell.setupCell(catalogImage: URL(string:"http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")!, catalogTitle: newCatalog?[indexPath.row].name ?? "")
            return cell
            
        case .stories:
            let newCatalog = catalog as? [StorySummary]
            //        let cat = catalog as? [ComicSummary]
            cell.setupCell(catalogImage: URL(string:"http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")!, catalogTitle: newCatalog?[indexPath.row].name ?? "")
            return cell
            
        default:
            print("dd")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 , height: collectionView.frame.size.width/2 + 70)
    }
}
