//
//  HeaderTableViewCell.swift
//  DeveloperMarvel
//
//  Created by mohamed on 20/01/2021.
//

import UIKit
import PureLayout

protocol HeaderTableViewCellBackDelegate {
    func onBackButton()
}
class HeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var characterImage: NetworkImageLoader!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView.newAutoLayout()
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        return indicator
    }()
    var delegate: HeaderTableViewCellBackDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupActivityIndicator()
    }
    
    @IBAction func onBackButton(_ sender: Any) {
        delegate?.onBackButton()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func setupCell(characterImageUrl : URL , name : String , description:String){
        nameLabel.text = name
        descriptionLabel.text = description
        characterImage.loadImage(from: characterImageUrl) { _ in
            DispatchQueue.main.async {
                self.removeActivityIndicator()
            }
        }
    }
    
}

private extension HeaderTableViewCell {
    
    private func setupActivityIndicator() {
        
        addSubview(activityIndicator)
        bringSubviewToFront(activityIndicator)
        
        activityIndicator.autoCenterInSuperview()
        activityIndicator.autoSetDimensions(to: CGSize(width: 22.0, height: 22.0))
        activityIndicator.startAnimating()
        
    }
    
    private func removeActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
}
