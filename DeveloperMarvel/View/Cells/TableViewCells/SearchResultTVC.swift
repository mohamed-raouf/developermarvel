//
//  SearchResultTVC.swift
//  DeveloperMarvel
//
//  Created by mohamed on 21/01/2021.
//

import UIKit

class SearchResultTVC: UITableViewCell {
    
    @IBOutlet weak var characterImage: NetworkImageLoader!
    @IBOutlet weak var characterName: UILabel!
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView.newAutoLayout()
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        return indicator
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupActivityIndicator()
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setupCell(searchResult:SearchResultModel){
        characterName.text = searchResult.name
        guard let url = searchResult.imageUrl  else {
            return
        }
        characterImage.loadImage(from: url) { _ in
            DispatchQueue.main.async {
                self.removeActivityIndicator()
            }
        }
    }
    
    
}


private extension SearchResultTVC {
    
    private func setupActivityIndicator() {
        
        addSubview(activityIndicator)
        bringSubviewToFront(activityIndicator)
        
        activityIndicator.autoCenterInSuperview()
        activityIndicator.autoSetDimensions(to: CGSize(width: 22.0, height: 22.0))
        activityIndicator.startAnimating()
        
    }
    
    private func removeActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
}
