//
//  HomeScreenPresenter.swift
//  DeveloperMarvel
//
//  Created by mohamed on 19/01/2021.
//

import Foundation
import UIKit

protocol HomeScreenPresenterDelegate: class {
    func loadCharacters(name: String?, at page: Int)
    func getData(characterData:(CharacterDataWrapper))
}

class HomeScreenPresenter {
    
    private weak var homeScreenPresenterDelegate: HomeScreenPresenterDelegate?
    private var repository : CharactersRepository
    
    init(repository:CharactersRepository){
        self.repository = repository
    }
    
    func setViewDelegate(_ homeScreenPresenterDelegate:HomeScreenPresenterDelegate?){
        self.homeScreenPresenterDelegate = homeScreenPresenterDelegate
    }
    
    
    func loadCharacters(name: String? = nil, at page: Int) {
        //        if !ReachabilityConnect.isConnectedToNetwork() {
        //            loadDataFromCache(name: name)
        //            return
        //        }
        
        
        
        repository.character(name : name ?? "" , page : page){ (result) in
            
            switch result {
            case let .success(characterData):
                self.homeScreenPresenterDelegate?.getData(characterData: characterData)
            case let .failure(error):
                print(error)
                
            }
        }
        
        
    }
    
    
    
    
}
