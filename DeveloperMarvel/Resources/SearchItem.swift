//
//  SearchItem.swift
//  MarvelApp
//
//  Created by mohamed on 19/01/2021.
//

import Foundation

class SearchItem {
    var attributedSearchTitle: NSMutableAttributedString?
    var allAttributedName : NSMutableAttributedString?
    
    var searchTitle: String
    
    
    public init(searchTitle: String) {
        self.searchTitle = searchTitle
        
    }
    
    public func getFormatedText() -> NSMutableAttributedString{
        allAttributedName = NSMutableAttributedString()
        allAttributedName!.append(attributedSearchTitle!)
        print(allAttributedName!)
        return allAttributedName!
    }
    
    public func getStringText() -> String{
        return "\(searchTitle)"
    }
    
}
