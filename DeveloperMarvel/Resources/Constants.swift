//
//  Constants.swift
//  MarvelApp
//
//  Created by mohamed on 17/01/2021.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>() 
struct Constants {
    
    static var link : String {
        get{
            #if DEBUG
            return "https://gateway.marvel.com:443"
            #else
            return "https://gateway.marvel.com:443"
            #endif
        }
    }
    
    static var baseUrl: String{
        get{
            return "\(link)/v1/public"
        }
    }
    
    
    
    
}
