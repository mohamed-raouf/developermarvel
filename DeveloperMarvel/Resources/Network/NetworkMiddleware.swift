

import Foundation
import Alamofire


struct ErrorReporter: Error {
    let errorString: String
}
class NetworkMiddleware: RequestInterceptor {

    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var urlRequest = urlRequest
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        completion(.success(urlRequest))
    }
    
    
    lazy var sessionManager: Alamofire.Session = {
        let configuration = URLSessionConfiguration.default
        #if Debug
        // print(configuration.httpAdditionalHeaders)
        // print(SessionManager.defaultHTTPHeaders)
        #endif
//        configuration.httpAdditionalHeaders = ["os": "ios"]
        configuration.timeoutIntervalForResource = 60
        configuration.timeoutIntervalForRequest = 60
        
        return Alamofire.Session(configuration: configuration , interceptor: self)
    }()
    

}
