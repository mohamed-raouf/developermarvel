
import Alamofire
import Japx

typealias NetworkCompletion<T> = (AFResult<T>) -> ()

protocol NetworkProtocol {
    func request<T>(_ request: URLRequestConvertible, decodeTo type: T.Type, completionHandler: @escaping NetworkCompletion<T>) where T: Codable
    func upload<T>(_ request: URLRequestConvertible, data: [UploadData], decodedTo type: T.Type, completionHandler: @escaping NetworkCompletion<T>) where T: Codable
    func download(_ url: String, fileName: String, type: String, completionHandler: @escaping (URL?) -> ())
    func cancelAllRequests()
}

class Network: RequestInterceptor {
    
    fileprivate let networkMiddleware = NetworkMiddleware()

    fileprivate lazy var manager: Session = networkMiddleware.sessionManager
       
    fileprivate func process<T>(response: AFDataResponse<Any>, decodedTo type: T.Type) -> AFResult<T> where T: Codable {
        switch response.result {
        case .success:
            
            guard let data = response.data else {
                return .failure(NSError.create(description: "Server Error.".localize))
            }
            
            debugPrint("=======DEBUG========🐰🐰=======NETWORK=====")
            debugPrint("Response URL: \(response.request?.url?.absoluteString ?? "")")
            let json = try? Japx.Decoder.jsonObject(with: data)
            print(JSON(json ?? [:]))
            debugPrint("=======DEBUG========🐰🐰=======NETWORK=====")

            
            do {
                if json == nil {
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(T.self, from: data)
                    debugPrint(responseModel)
                    return .success(responseModel)
                }
                let data = try JapxDecoder()
                    .decode(type, from: data)
                return .success(data)
            } catch {
                
                #if DEBUG
                debugPrint(error)
                #endif
                
                return .failure(NSError.create(description: "Server Error.".localize))
            }
            
        case .failure(let error):
            
            #if DEBUG
            debugPrint("#DEBUG#", error.localizedDescription)
            #endif
            
            if error.localizedDescription.contains("JSON") {
                return .failure(NSError.create(description: "Server Error.".localize))
            }
            
//            return .failure(error)
            return .failure(NSError(domain: "", code: 000, userInfo: ["message": error.localizedDescription]))
        }
    }
    
    func cancelAllRequests() {
        manager.session.getAllTasks { tasks in tasks.forEach { $0.cancel() } }
    }
}

extension Network: NetworkProtocol {
    func request<T>(_ request: URLRequestConvertible, decodeTo type: T.Type, completionHandler: @escaping (AFResult<T>) -> ()) where T: Codable {
        
        manager.request(request).responseJSON {[weak self] response in
            guard let self = self else { return }
            print(response)
//            self.convertAnyJSON_APIExample(sentence: "{\"data\": [{\"type\": \"question\",\"id\": \"76\",\"relationships\": {\"answers\": {\"data\": [{\"id\": \"new\",\"type\": \"answer\"}]}}}],\"included\": [{\"type\": \"answer\",\"id\": \"new\",\"attributes\": {\"answer_id\": 18,\"answer_texts\": false}}]}")
            completionHandler(self.process(response: response, decodedTo: type))
        }
    }
    
    func convertAnyJSON_APIExample(sentence: String) {
        let data = sentence.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
            {
                let json = try? Japx.Decoder.jsonObject(with: data)
               print(JSON(json ?? [:]))
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
        
    }
    
    func download(_ url: String, fileName: String, type: String, completionHandler: @escaping (URL?) -> ()) {
        
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .downloadsDirectory, in: .allDomainsMask)[0]
            documentsURL.appendPathComponent("\(fileName).\(type)")
            return (documentsURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        manager.download(url, to: destination)
            .debugLog()
            .responseData { response in
                if let destinationUrl = response.fileURL {
                    completionHandler(destinationUrl.absoluteURL)
                    print("destinationUrl \(destinationUrl.absoluteURL)")
                    
                    
                } else {
                    completionHandler(nil)
                }
        }
        .downloadProgress { (progress) in
            #if DEBUG
            print(String(format: "%.1f", progress.fractionCompleted * 100))
            #endif
        }
    }
    
    func upload<T>(_ request: URLRequestConvertible, data: [UploadData], decodedTo type: T.Type, completionHandler: @escaping (AFResult<T>) -> ()) where T: Decodable, T: Encodable {
    
        
        manager.upload(multipartFormData: { multipart in

            data.forEach {
                multipart.append($0.data, withName: $0.name+"[]", fileName: $0.fileName, mimeType: $0.mimeType)
            }

            for (key, value) in request.parameters ?? [:] {
                multipart.append("\(value)".data(using: .utf8)!, withName: key)
            }

        }, with: request.urlRequest!)
              .uploadProgress(queue: .main, closure: { progress in
                  //Current upload progress of file
                
                #if DEBUG
                print(String(format: "%.1f", progress.fractionCompleted * 100))
                #endif
                
              })
              .responseJSON(completionHandler: {[weak self] response in
                  guard let self = self else { return }
                  completionHandler(self.process(response: response, decodedTo: type))
              })

    }
}
