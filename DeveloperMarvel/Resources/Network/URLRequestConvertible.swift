

import Foundation
import Alamofire

protocol URLRequestConvertible: Alamofire.URLRequestConvertible {
    
    var method: HTTPMethod { get }
    var parameters: [String: Any]? { get }
    var url: URL { get }
    // var pageUrl: URL { get }
    var encoding: ParameterEncoding { get }
}


class ActionRequest: URLRequestConvertible {
    
    var action: Action
    
    var method: HTTPMethod {
        return HTTPMethod(rawValue: action.method!) ?? .get
    }
    
    var parameters: [String: Any]? {
        return nil
    }
    
    var url: URL {
        return URL(string: action.endpointURL ?? "www.google.com") ?? URL(string: "www.google.com")!
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    init(_ action: Action) {
        self.action = action
    }
}
