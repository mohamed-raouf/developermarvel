//
//  CharactersRepository.swift
//  MarvelApp
//
//  Created by mohamed on 17/01/2021.
//

import Foundation
import Alamofire
//import LaravelEchoIOS

typealias AFResult<T> = Result<T, NSError>

protocol CharactersRepositoryProtocol: class {
    
    func character(name:String , page : Int ,completionHandler: @escaping(AFResult<CharacterDataWrapper>)->())
}

class CharactersRepository: CharactersRepositoryProtocol {
   
    
    func character(name : String , page : Int ,completionHandler: @escaping(AFResult<CharacterDataWrapper>)->()) {
        let route = CharactersRouter.character(name : name , page : page)
        // pair to pair message
        network.request(route, decodeTo: CharacterDataWrapper.self, completionHandler: completionHandler)
    }

    
    
    private var network: NetworkProtocol
    
    init(network: NetworkProtocol) {
        self.network = network
    }
}


extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

enum UIUserInterfaceIdiom : Int {
    case unspecified
    case phone // iPhone and iPod touch style UI
    case pad   // iPad style UI (also includes macOS Catalyst)
}
