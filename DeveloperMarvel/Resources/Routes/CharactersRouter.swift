//
//  CharactersRouter.swift
//  MarvelApp
//
//  Created by mohamed on 17/01/2021.
//


import Foundation
import Alamofire

enum CharactersRouter: URLRequestConvertible {
    
//    {
//        var parm = [URLQueryItem]()
////        ss.append(URLQueryItem(name: "offset", value: "\(20)"))
//        return parm
//    }
    case character(name:String , page:Int)
    
    
    var method: HTTPMethod {
        switch self {
        case .character:
            return .get
        default:
            return .post
        }
    }
    
//    mutating func addParm(page : Int){
//        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
//        var customQueryItems = [URLQueryItem]()
//        customQueryItems.append(URLQueryItem(name: "offset", value: "\(20)"))
////        customQueryItems.append(URLQueryItem(name: "offset", value: "\(page * limit)"))
//
//        let com = getUrl()
////        com.queryItems = com.queryItems + customQueryItems
////        getUrl().components = getUrl().components?.queryItems + customQueryItems
//    }
    var parameters: [String: Any]? {
        switch self {
        case .character(let name , let page):
            return ["name" : name ,"offset": page]
        default:
            return nil
        }
    }
    
    var url: URL {
        switch self {
        case .character:
            return URL(string: "\(Constants.baseUrl)/")!.appendingPathComponent(path)
        default:
            return URL(string: "\(Constants.baseUrl)/")!.appendingPathComponent(path)
        }
    }

    var pageUrl: URL {
        return url
    }
    
    
    var path: String {
        switch self {
        case .character(let name , let page):
//            customQueryItems.append(URLQueryItem(name: "offset", value: "\(20)"))
            return "characters"
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .character:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
//        var components = URLComponents(url: getUrl(), resolvingAgainstBaseURL: true)
        var customQueryItems = [URLQueryItem]()
        
//        var urlRequest = URLRequest(url: getUrl())
        let obj = parameters! as NSDictionary
           for (key, value) in obj {
               print("Property:\(key) \(value)")
            if key as! String == "offset" {
                if value as! Int != 0 {
                    customQueryItems.append(URLQueryItem(name: "\(key)", value: "\(value)"))
                }
            }
            if key as! String == "name" {
                if value as! String != "" {
                    customQueryItems.append(URLQueryItem(name: "\(key)", value: "\(value)"))
                }
            }
//            customQueryItems.append(URLQueryItem(name: "\(key)", value: "\(value)"))
           }
        
        let timestamp = "\(Date().timeIntervalSince1970)"
        let hash = "\(timestamp)\(KeyCenter.instance.privateKey)\(KeyCenter.instance.apiKey)".md5

        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        let commonQueryItems = [
            URLQueryItem(name: "ts", value: timestamp),
            URLQueryItem(name: "hash", value: hash),
            URLQueryItem(name: "apikey", value: KeyCenter.instance.apiKey)
        ]

        components?.queryItems = commonQueryItems + customQueryItems
        
        var urlRequest = URLRequest(url:(components?.url!)!)

        urlRequest.httpMethod = method.rawValue
        // to send parameters in body request
        //urlRequest.httpBody = parameters
        urlRequest.timeoutInterval = 90000000000
        return try encoding.encode(urlRequest, with: nil)
    }
    
    
}
