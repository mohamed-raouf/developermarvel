//
//  BaseView.swift
//  Torch
//
//  Created by Youssef on 8/11/19.
//  Copyright © 2019 Youssef. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol BaseViewProtocol: class {
    func startLoading()
    func stopLoading()
    func showAlert(with message: String , selfDismissing: Bool)
}

extension BaseViewProtocol where Self: UIViewController {
    
    func startLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        startLoading()
    }
    
    func stopLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        stopLoading()
    }
    
    func showAlert(with message: String , selfDismissing: Bool = true ) {
        showAlert(message: message ,selfDismissing: selfDismissing )
//        HelperUIClass.showAlert(viewController: self, text: message)
    }
    func showPopupAlert(with message: String){
        showPopupAlert(with: message)
    }
}
