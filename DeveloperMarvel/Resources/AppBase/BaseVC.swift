//
//  BaseViewController.swift
//  BaseProject
//
//  Created by Youssef on 7/28/19.
//  Copyright © 2019 Youssef. All rights reserved.
//

import UIKit
import Alamofire

class BaseVC<T: BaseView>: BaseController {
    
    final var mainView = T()
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var frame: CGRect = (self.navigationController?.navigationBar.frame)!
        let orient = UIApplication.shared.statusBarOrientation

        switch orient {
        case .portrait:

            frame.size.height = 64
            frame.size.width = self.view.frame.width
        default:

            frame.size.height = 52
            frame.size.width = self.view.frame.width
        }
        
        if let titley = titley {
            navigationItem.title = titley
        }
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        var frame: CGRect = (self.navigationController?.navigationBar.frame)!

         coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in

             let orient = UIApplication.shared.statusBarOrientation

             switch orient {
             case .portrait:

                 frame.size.height = 64
                 frame.size.width = self.view.frame.width
             default:

                 frame.size.height = 72
                 frame.size.width = self.view.frame.width
//                 let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.CairoRegular(of: 20)]
//                 self.navigationController?.navigationBar.largeTitleTextAttributes = attributes
             }
         }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
             print("rotation completed")
             self.navigationController?.navigationBar.frame = frame
         })
    }

    
    deinit {
        removeNotificationsObservers()
    }
}

class BaseController: UIViewController, BaseViewProtocol {
    
//    final lazy var repository: RepositoryProtocol = Repository(view: self)
    
    var titley: String?
    
//    let backView = UIView(backgroundColor: .white)
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.addSubview(backView)
//        backView.centerXInSuperview()
//        backView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//        backView.widthAnchorWithMultiplier(multiplier: 1)
//        backView.heightAnchorConstant(constant: 35)
        addTitleImage()
    }
    
    func addTargets() {
        
    }
    
    func configView() {
        
    }
    
    deinit {
        removeNotificationsObservers()
    }
}


class BasePresenter {
    
//    var network: NetworkProtocol = Network()
    
//    func handleRequestResponse<U: BaseCodable>(view: BaseViewProtocol?,_ result: AFResult<U>) -> U? {
//        view?.stopLoading()
//        switch result {
//        case .success(let data):
//            if data.errors == nil {
//                return data
//            } else if let errors = data.errors {
//                if errors.count > 0 {
//                    var errorString = ""
//                    for error in errors{
//                        errorString.append(error.detail ?? "Error".localize)
//
//                        errorString.append("\n")
//                    }
//                    let error = errors.first
//                    if error?.title == "unauthorized_action" && error?.status == 403 {
//                        AuthService.instance.restartAppAndRemoveUserDefaults()
//                    } else {
//                        view?.showAlert(with: errorString , selfDismissing: false)
//                    }
//                }
//                return nil
//            } else {
//                view?.showAlert(with: "Error".localize , selfDismissing: true)
//                return nil
//            }
//        case .failure(let error):
//            view?.showAlert(with: error.localizedDescription.localize , selfDismissing: true)
//
//            return nil
//        }
//    }
}

class BaseView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAppearance()
        setupView()
        addTargets()
        configSubViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func setupView() {
        
    }
    
    func setupAppearance() {
        backgroundColor = #colorLiteral(red: 0.911196053, green: 0.9328929782, blue: 0.9679442048, alpha: 1)
    }
    func setupwhiteappearance(){
        backgroundColor = #colorLiteral(red: 1, green: 0.9878020883, blue: 0.98788625, alpha: 1)

    }
    
    func configSubViews() {
        
    }
    
    func addTargets() {
        
    }
}

class ListView: BaseView {
    
    let flowLayout = UICollectionViewFlowLayout()
    
    lazy var collectionView: UICollectionView = {
        let layout = flowLayout
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.alwaysBounceVertical = true
        return collectionView
    }()
    
//    override func setupView() {
//        addSubview(collectionView)
//        collectionView.fillSuperviewSafeArea(padding: .init(top: 20, left: 20, bottom: 2, right: 20))
//    }
    
    func setDelegates(_ delegate: Any) {
        collectionView.delegate = delegate as? UICollectionViewDelegate
        collectionView.dataSource = delegate as? UICollectionViewDataSource
    }
}
