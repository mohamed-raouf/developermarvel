//
//  BaseCell.swift
//  Son App
//
//  Created by Youssef on 1/24/20.
//  Copyright © 2020 Youssef. All rights reserved.
//

import UIKit

class BaseCollectionViewCell<U: Codable>: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func setupCell() {
        backgroundColor = .white
    }
    
    var data: U!
    var index: Int!
    var cellSize: CGSize!
    
    func configCell(_ data: U?) {
        
    }
}
