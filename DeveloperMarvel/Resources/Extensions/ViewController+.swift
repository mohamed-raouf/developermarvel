//
// UIViewController.swift
//
//
//  Created by Youssef on 9/16/18.
//  Copyright © 2018 Youssef. All rights reserved.
//

import UIKit
import Network

extension UIViewController {
    
    func presentModelyVC(_ vc: UIViewController) {
        vc.definesPresentationContext = true
        if #available(iOS 13, *) {
            // vc.modalPresentationStyle = .fullScreen
        } else {
            vc.modalPresentationStyle = .custom
        }
        present(vc, animated: true, completion: nil)
    }
    
    func push(_ vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func push(after: Double,_ vc: UIViewController) {
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func popMe() {
        navigationController?.popViewController(animated: true)
    }
    
    func popMe(after: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func popToRoot() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func popToRoot(after: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    func monitorNetwork(){
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reconnect_internet"), object: nil)
                }
            }
        }
        let queue = DispatchQueue(label: "Network")
        monitor.start(queue: queue)
    }
    func configNavBar(title: String? = nil, isLarge: Bool, backgroundColor: UIColor = .white) {
        navigationItem.title = title
        navigationController?.navigationBar.prefersLargeTitles = isLarge
//        let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.CairoBold(of: 22)]
//        navigationController?.navigationBar.largeTitleTextAttributes = attributes
//        navigationController?.navigationBar.titleTextAttributes = attributes
        // navigationController?.navigationBar.isOpaque = true
        navigationController?.navigationBar.backgroundColor = backgroundColor
//        navigationController?.navigationBar.tintColor = .mainNav
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layer.masksToBounds = false
        navigationController?.navigationBar.layer.cornerRadius = 20
        navigationController?.navigationBar.isTranslucent = true
        addShadowToNavBar()
        // navigationController?.navigationBar.applySketchShadow()
        // navigationController?.navigationBar.clipsToBounds = true
        // navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    func addShadowToNavBar() {
        navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        navigationController?.navigationBar.layer.shadowOpacity = 0.8
        navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 7)
        navigationController?.navigationBar.layer.shadowRadius = 5
    }
    
    func removeShadowToNavBar() {
        navigationController?.navigationBar.layer.shadowColor = UIColor.clear.cgColor
    }
    
    func addTitleImage() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFill
//        imageView.viewCornerRadius = 5
        imageView.clipsToBounds = true
//        imageView.withSize(.init(all: 30))
//        imageView.load(with: AuthService.instance.userImage)
        navigationItem.titleView = imageView
    }
    
    @objc
    func goToNotifications() {
//        Constants.notificationNo = 0
//        push(NotificationViewController.create())
    }
    
    func showAlert(title: String? = "", message: String?, selfDismissing: Bool = true, time: TimeInterval = 1) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        alert.title = title
        alert.message = message
        
        if !selfDismissing {
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        }
        
        present(alert, animated: true)
        
        if selfDismissing {
            Timer.scheduledTimer(withTimeInterval: time, repeats: false) { t in
                t.invalidate()
                alert.dismiss(animated: true)
            }
        }
    }
    
    
    func showPopupAlert(with message:String){
        let alert = UIAlertController(title: "Done", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (alertAction) in
            debugPrint("Ok")
            self.navigationController?.popViewController(animated: true)
        }))
        
        present(alert , animated: true)
    }
    func transperantNavBar() {
        navigationController?.navigationBar.isOpaque = true
        navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.tintColor = .mainColor
        navigationController?.navigationBar.topItem?.title = ""
    }
    
    func navbarWithdismiss() {
        let exitButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(dismissMePlease))
        navigationItem.rightBarButtonItem = exitButton
    }
    
    @objc func dismissMePlease() {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func setStatusBarColor() {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: (UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = #colorLiteral(red: 0.9998950362, green: 1, blue: 0.9998714328, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor.red
        }
    }
}

extension UIViewController {
    func addNotificationObserver(name: Notification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: name, object: nil)
    }
    
    func removeNotificationObserver(name: Notification.Name) {
        NotificationCenter.default.removeObserver(self, name: name, object: nil)
    }
    
    func removeNotificationsObservers() {
        NotificationCenter.default.removeObserver(self)
    }
}

extension UIViewController {
    
    var toNavigation: UINavigationController {
        return UINavigationController(rootViewController: self)
    }
}

extension BaseVC {
    func dissmissView(view: UIView) {
        if #available(iOS 13, *) {
            return
        } else {
            let swip = UISwipeGestureRecognizer(target: self, action:#selector(dismissMePlease))
            view.addGestureRecognizer(swip)
            swip.direction = .down
        }
    }
}

extension BaseVC {
    var isIOSTherteen: Bool {
        if #available(iOS 13, *) {
            return true
        } else {
            return false
        }
    }
}


extension UIViewController {
//    open override func awakeFromNib() {
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//    }
}

extension UIViewController {
    func addChildViewController(childViewController: UIViewController, childViewControllerContainer: UIView, comp: (() -> ())? = nil) {
        removeChildViewControllers()
        addChild(childViewController)
        
        childViewControllerContainer.addSubview(childViewController.view)
        childViewController.view.frame = childViewControllerContainer.bounds
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        childViewController.didMove(toParent: self)
        
        comp?()
    }
    
    func removeChildViewControllers() {
        if children.count > 0 {
            for viewContoller in children {
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
    }
}
